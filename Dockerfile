FROM archlinux:base AS thebase

RUN \
  useradd -m user && \
  usermod -aG wheel user && \
  `# allow any user to sudo without password` \
  echo "%wheel ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers
# https://www.reddit.com/r/archlinux/comments/494c84/comment/d0pfn6y/
RUN sed -i "s/#MAKEFLAGS.*/MAKEFLAGS=\"-j$(nproc)\"/" /etc/makepkg.conf
# https://www.reddit.com/r/archlinux/comments/494c84/comment/d0oytzs/
RUN sed -i 's/\(COMPRESS[A-Z0-9]*\)=(.*/\1=(cat)/' /etc/makepkg.conf

RUN pacman -Sy --noconfirm archlinux-keyring
RUN pacman -Syyu --noconfirm

USER root
# core apps
RUN pacman -S --noconfirm \
    awesome \
    base-devel \
    dmenu \
    firefox \
    git \
    inetutils \
    kitty \
    net-tools \
    python3 \
    supervisor \
    vim \
    which \
    x11vnc \
    xorg-apps \
    xorg-server \
    xorg-server-xvfb \
    xorg-xinit

USER user
RUN \
  cd /tmp && \
  git clone https://aur.archlinux.org/yay.git && \
  cd yay && \
  makepkg -si --noconfirm && \
  cd .. && \
  rm -fr yay/

USER root
# noVNC setup
WORKDIR /usr/share/noVNC
COPY noVNC/ ./
RUN ln -s /usr/share/noVNC/vnc.html /usr/share/noVNC/index.html
WORKDIR /usr/share/websockify
COPY websockify/ ./
RUN python setup.py install


FROM thebase

USER user
# only apps, not "base stuff"
RUN \
  echo 'source ~/.config/extra.bashrc' >> ~/.bashrc && \
  yay -S --noconfirm \
    htop \
    nodejs \
    npm \
    nvm \
  && \
  node --version && \
  npm --version && \
  npm -g install yarn && \
  yarn --version

COPY supervisord.conf /etc/
COPY awesome/rc.lua /home/user/.config/awesome/rc.lua
COPY extra.bashrc /home/user/.config/
EXPOSE 8083
# EXPOSE 5900  # FIXME breaks startup

USER user
WORKDIR /home/user

CMD ["/usr/bin/supervisord"]
