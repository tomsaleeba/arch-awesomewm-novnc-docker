```
docker-compose up
```

..then open http://localhost:8083


Credit for original project: https://github.com/fennerm/arch-i3-novnc-docker

Remmina is a good vnc client, with client-side scaling.


## FIXME
- can I use Firefox kiosk mode to ignore all the other keystrokes?
- add vim config
- add oh-my-zsh (and zsh?)
- add password to vnc
- add TLS to vnc
- add vscode
- why does makepkg take soooo much longer inside the container? Like 10 mins. Looks single
  threaded. I tried modifying MAKEFLAG and when that was invalid, it gave an error, so it
  reads it, but maybe it's still not being used?
- get ctrl-w and ctrl-shift-pg(up/dn) to work inside the window
- caps lock doesn't work
- key repeat doesn't work
- add kitty split bindings
- add command execution time to bash prompt
- auto-magic port forwarding


# Requirements
- hosts an IDE for collaborative programming work
- able to support at least 2 concurrent users
- users can work together: locked to viewing the same thing and the same time, and both
  can control. Can be "screen share" style or "google docs" style
- users can work separately: more like separate SSH sessions
- can run any toolchain: nodejs, python, etc
- choice of IDE: vim, vscode
- terminal is accessible
- access via browser for the whole IDE
- if "screen share" style:
    - minimal latency local editor can be connected
    - any web server is separately exposed, so it can be hit directly
    - web browser runs server-side so "inspect" and "debug" activity is also shared


# Reviews of other platforms
## codesandbox.io
- has "live sessions" feature
    - It works a little bit
    - You can follow avatars and it takes you into files (but not terminals) and follows
      line focus. It messes up the default view for markdown files
    - lots of little things don't work, like newly connected users don't have their avatar
      show up, so you have to reload the page to see them
    - web browsing is not sync'd
    - it's more of a "work on the same code base at the same time" feature than a "let me
      show you what I'm seeing" feature
- typing in a terminal is slow, probably hosted in america
- have to use crusty cloud IDE, no vim bindings
- generally unhappy about the experience after having to do it

## repl.it
- has vim-mode, but esc mode is being munged by vimium I think
- has editor, web-view and shell/repl all in one view
- ssh in paid plans to use local vim
- multiplayer/invite other editors (haven't actually tried it)

## Github codespaces
- https://docs.github.com/en/codespaces/overview
- collab needs vscode "live share"

## AWS cloud9
https://aws.amazon.com/cloud9/

## codeanywhere.com

## codepen.io
- only frontend apps

## www.codetogether.com
- only full IDE integration
